<?php
  require_once("includes/phpscripts/config.php");
  
  if(isset($_POST['name'])){
    $name = $_POST['name'];
    $email = $_POST['email'];
    $message = $_POST['message'];
    $street = $_POST['street'];
    $direct = "thankyou.php";
    if($street === "") {
      $sendMail = submitMessage($name, $email, $message, $direct);
      //echo "Street Is empty";
    }
  }

?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>GR Media</title>
    <link rel="icon" type="image/svg" href="img/gr-logo-opt.png"/>
    <link rel="stylesheet" href="css/foundation.css"/>
    <link rel="stylesheet" href="css/app.css"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">
  </head>
  <body>
  <h1 class="hidden">Home Page</h1>
  <?php
    include('includes/nav.html');
  ?>
    <div id="container"><br>
      <h2>Contact</h2>
      <p>Please take a minute to fill out the form below. Responses may take up to two hours or more.</p><br>
        <div class="row">
          <div class="small-12 large-8 columns">
            <form action="contact.php" method="post"> 
              <label><p>Name: </p></label><input name="name" type="text" size="21" maxlength="30" />
              <label><p>Email: </p></label><input name="email" type="text" size="21" maxlength="30" />
              <label for="street"><p>Street: </p></label><input class="street" name="street" type="text" size="21" maxlength="30" />
              <label for="message"><p>Message: </p></label><textarea name="message"></textarea>
              <input name="submit" type="submit" value="Send"/>
            </form>
          </div>
        </div><br>
        <p>Visit my social media pages!</p>
        <div class="socialMedia">
          <ul class="row">
            <li class="small-3 medium-2 large-1 columns end"><a href="http://www.facebook.com/garethrichardsonmedia"><img src="img/facebook-logo-small.png" alt="Facebook"></a></li>
            <li class="small-3 medium-2 large-1 columns end"><a href="http://www.twitter.com/garethrichardsonmedia"><img src="img/twitter-logo-small.png" alt="Twitter"></a></li>
            <li class="small-3 medium-2 large-1 columns end"><a href="http://www.instagram.com/garethrichardsonmedia"><img src="img/instagram-logo-small.png" alt="Instagram"></a></li>
            <li class="small-3 medium-2 large-1 columns end"><a href="http://www.youtube.com/c/garethrichardsonmedia"><img src="img/youtube-logo-small.png" alt="YouTube"></a></li>
          </ul>
        </div><br>
    </div>
	<!--DON'T DELETE!!! -->
    <script src="js/vendor/jquery.min.js"></script>
    <script src="js/vendor/what-input.min.js"></script>
    <script src="js/foundation.js"></script>
    <script src="js/app.js"></script>
    <!-- DON'T DELETE!!! -->
  </body>
</html>
