<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>GR Media</title>
    <link rel="icon" type="image/svg" href="img/gr-logo-opt.png"/>
    <link rel="stylesheet" href="css/foundation.css"/>
    <link rel="stylesheet" href="css/app.css"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">
  </head>
  <body>
  <h1 class="hidden">Home Page</h1>
  <?php
    include('includes/nav.html');
  ?>
    <div id="container"><br>
      <h2>Thank You!</h2>
      <p>A response can typically take up to two hours. Be sure to check your inbox often.</p><br>
      <a href="index.php">GO BACK TO THE HOME PAGE</a>
    </div>
	<!--DON'T DELETE!!! -->
    <script src="js/vendor/jquery.min.js"></script>
    <script src="js/vendor/what-input.min.js"></script>
    <script src="js/foundation.js"></script>
    <script src="js/app.js"></script>
    <!-- DON'T DELETE!!! -->
  </body>
</html>
