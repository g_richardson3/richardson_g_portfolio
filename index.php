<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>GR Media</title>
    <link rel="icon" type="image/svg" href="img/gr-logo-opt.png"/>
    <link rel="stylesheet" href="css/foundation.css"/>
    <link rel="stylesheet" href="css/app.css"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">
  </head>
  <body>
  <h1 class="hidden">Home Page</h1>
  <?php
    include('includes/nav.html');
  ?>
    <div id="containerSpace">
      <div class="row" id="homeArticle">
        <div class="small-12 columns">
          <article class="homeDesc">
            <h2 class="fade-in one">Hello!</h2>
            <p class="fade-in two">My name is Gareth Richardson.</p>
            <p class="fade-in three">I am a multimedia designer. I specialize in everything from graphic design, to UI/UX design, to motion design, to video production, and 3D animation &amp; character design. I also produce music as a hobby. My love for all digital mediums has driven my passion to pursue a career and a lifestyle in the field of digital media. I have taken advantage of countless opportunities throughout my life involving many forms of digital media. Everything from telecommunications (radio &amp; TV), to photo/video editing, to producing music digitally, to many forms of computer programming.</p>
          </article>
        </div>
      </div>
    </div>
	<!--DON'T DELETE!!! -->
    <script src="js/vendor/jquery.min.js"></script>
    <script src="js/vendor/what-input.min.js"></script>
    <script src="js/foundation.js"></script>
    <script src="js/app.js"></script>
    <!-- DON'T DELETE!!! -->
  </body>
</html>
