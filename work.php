<?php
  
  ini_set('display_errors',1);
  error_reporting(E_ALL);

  require_once('includes/phpscripts/init.php');

  if(isset($_GET['filter'])) {
    $tbl1 = "tbl_work";
    $tbl2 = "tbl_type";
    $tbl3 = "tbl_work_type";
    $col1 = "work_id";
    $col2 = "type_id";
    $col3 = "type_name";
    $filter = $_GET['filter'];
    $getWork = filterType($tbl1, $tbl2, $tbl3, $col1, $col2, $col3, $filter);
  } else {
    $tbl = "tbl_work";
    $getWork = getAll($tbl);
  }
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>GR Media</title>
    <link rel="icon" type="image/svg" href="img/gr-logo-opt.png"/>
    <link rel="stylesheet" href="css/foundation.css"/>
    <link rel="stylesheet" href="css/app.css"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">
  </head>
  <body>
  <h1 class="hidden">Home Page</h1>
  <?php
    include('includes/nav.html');
  ?>
    <div id="container"><br>
      <div class="row">
      <?php
        if(!is_string($getWork)){
        while($row = mysqli_fetch_array($getWork)){
          echo"<div class=\"small-12 medium-6 large-4 columns end\">
          <h2 class=\"workHeader\">{$row['work_title']}</h2>
          <a class=\"workMore\" href=\"details.php?id={$row['work_id']}\"><img src=\"img/small-thumb/{$row['work_thumbsmall']}.jpg\" alt=\"{$row['work_title']}\"></a><br><br>
          </div>";}
        }else{
          echo "<p>{$getWork}</p>";
        }
      ?>
      </div>
      <?php
        include('includes/footer.html');
      ?>
    </div>
	<!--DON'T DELETE!!! -->
    <script src="js/vendor/jquery.min.js"></script>
    <script src="js/vendor/what-input.min.js"></script>
    <script src="js/foundation.js"></script>
    <script src="js/app.js"></script>
    <!-- DON'T DELETE!!! -->
  </body>
</html>
