-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 04, 2018 at 01:47 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_portfolio`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_type`
--

CREATE TABLE `tbl_type` (
  `type_id` smallint(5) UNSIGNED NOT NULL,
  `type_name` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_type`
--

INSERT INTO `tbl_type` (`type_id`, `type_name`) VALUES
(1, 'graphic'),
(2, 'web'),
(3, 'motion');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_work`
--

CREATE TABLE `tbl_work` (
  `work_id` smallint(5) UNSIGNED NOT NULL,
  `work_title` varchar(150) NOT NULL,
  `work_desc` varchar(700) NOT NULL,
  `work_video` varchar(200) NOT NULL,
  `work_thumbbig` varchar(130) NOT NULL,
  `work_thumbsmall` varchar(130) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_work`
--

INSERT INTO `tbl_work` (`work_id`, `work_title`, `work_desc`, `work_video`, `work_thumbbig`, `work_thumbsmall`) VALUES
(1, 'Mechanical Heart Animation', 'A 10 second animation that I created in Cinema 4D. The mechanical heart is placed into another heart made of broken glass. Reflective texturing applied to all shapes and parts. Slight motion blur is added to create extra effect and increase the overall quality and authenticity of the video.', 'vimeo.com/201010489', 'mechanical-heart-animation-big', 'mechanical-heart-animation-small'),
(2, 'Simple Character Walking Animation', 'A simple character made up of simple shapes that I created in Cinema 4D. Brightly lit colours and scenery make up for the simplicity of the design. I plan on building upon this character and including more complex animations in the future!', 'vimeo.com/249007565', 'simple-character-walk-animation-big', 'simple-character-walk-animation-small'),
(3, 'Biotronix Intro', 'A music video intro that I created for Biotronix Hard Dance\'s YouTube channel using Cinema 4D and composited in After Effects. Lots of quick movements were initiated to sync with the fast paced audio used in the video.', 'vimeo.com/249007791', 'biotronix-intro-big', 'biotronix-intro-small'),
(4, 'Biotronix Video Template', 'A basic, but sleek looking music video template that accompanies the intro used for Biotronix Hard Dance\'s YouTube channel.', 'N/A', 'biotronix-video-template-big', 'biotronix-video-template-small'),
(5, 'LEDC Mobile Designs', 'Mobile UX designs that I created in Photoshop for London Economic Development Corporation\'s website as a school project. A basic and simple design.', 'N/A', 'ledc-mobile-designs-big', 'ledc-mobile-designs-small'),
(6, 'Autism Ontario Mobile Designs', 'Mobile UX designs that I created in Photoshop for Autism Ontario\'s website as a school project. A basic and simple design with added navigation menu animations.', 'N/A', 'autism-ontario-mobile-designs-big', 'autism-ontario-mobile-designs-small'),
(7, 'Gareth Richardson Media Cover Art', 'YouTube cover art that I created for my social media platforms. I like space!', 'N/A', 'gareth-richardson-media-cover-art-big', 'gareth-richardson-media-cover-art-small'),
(8, 'CZquare 20 Outro', 'A video outro that I created in Cinema 4D and composited in After Effects for CZquare 20\'s YouTube channel. Created a virtual office setup with an animated speaker that pulses in sync to the music from the outro.', 'vimeo.com/249007101', 'czquare-20-outro-big', 'czquare-20-outro-small'),
(9, 'Virtual Therapy Room', 'A virtual therapy room that I created in Cinema 4D and composited in After Effects. This video is also encoded with 360 video metadata and can be experienced through select Virtual Reality headsets and devices.', 'vimeo.com/249013198', 'virtual-therapy-room-big', 'virtual-therapy-room-small'),
(10, 'Various Album Art Volume 2', 'A collection of Album Artwork I have designed for my own music.', 'N/A', 'various-album-art-collage-2-big', 'various-album-art-collage-2-small'),
(11, 'Autism Ontario Print Ads', 'Print ads that I created in Photoshop for Autism Ontario as a part of a school project. The ads contain basic design elements and easy-to-read typefaces.', 'N/A', 'autism-ontario-print-ads-collage-big', 'autism-ontario-print-ads-collage-small'),
(12, 'Pool Table 3D Render', 'A pool table with pool balls that I created In Cinema 4D and enhanced in Photoshop.', 'N/A', 'pool-table-3d-render-big', 'pool-table-3d-render-small'),
(13, 'Mobile Music App UX/UI Design', 'A mobile music app UX/UI design that I created in Photoshop for NoiseTacticz as a part of a school project. Includes a full design of a media player.', 'N/A', 'mobile-music-app-design-big', 'mobile-music-app-design-small'),
(14, 'Various Album Art Volume 1', 'A collection of Album Artwork I have designed for my own music.', 'N/A', 'various-album-art-collage-1-big', 'various-album-art-collage-1-small'),
(15, 'Realistic 3D Marble Render', 'A realistic marble render that I created in Cinema 4D and enhanced in Photoshop. Attempting to created the most realistic render possible with a minimal amount of objects and textures. Realism is key when making 3D models!', 'N/A', 'realistic-3d-marble-render-big', 'realistic-3d-marble-render-small'),
(16, 'Studio Monitor 3D Render', 'A studio monitor that I created in Cinema 4D and enhanced in Photoshop. Trying to model and replicate the studio monitors that are on my desk!', 'N/A', 'studio-monitor-3d-render-big', 'studio-monitor-3d-render-small');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_work_type`
--

CREATE TABLE `tbl_work_type` (
  `work_type_id` tinyint(3) UNSIGNED NOT NULL,
  `work_id` tinyint(4) NOT NULL,
  `type_id` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_work_type`
--

INSERT INTO `tbl_work_type` (`work_type_id`, `work_id`, `type_id`) VALUES
(1, 1, 3),
(2, 2, 3),
(3, 3, 3),
(4, 4, 1),
(5, 5, 2),
(6, 6, 2),
(7, 7, 1),
(8, 8, 3),
(9, 9, 3),
(10, 10, 1),
(11, 11, 1),
(12, 12, 3),
(13, 13, 2),
(14, 14, 1),
(15, 15, 3),
(16, 16, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_type`
--
ALTER TABLE `tbl_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `tbl_work`
--
ALTER TABLE `tbl_work`
  ADD PRIMARY KEY (`work_id`);

--
-- Indexes for table `tbl_work_type`
--
ALTER TABLE `tbl_work_type`
  ADD PRIMARY KEY (`work_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_type`
--
ALTER TABLE `tbl_type`
  MODIFY `type_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_work`
--
ALTER TABLE `tbl_work`
  MODIFY `work_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_work_type`
--
ALTER TABLE `tbl_work_type`
  MODIFY `work_type_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
